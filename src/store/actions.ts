import { ChangeNameAction } from "./name";

export enum ActionTypes {
    changeName,
}

export type Actions = ChangeNameAction;

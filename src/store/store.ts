import { combineReducers } from "redux";
import { nameReducer } from "./name";

export const rootReducer = combineReducers({
    name: nameReducer,
});

export type State = ReturnType<typeof rootReducer>;

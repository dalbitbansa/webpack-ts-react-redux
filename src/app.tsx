import * as React from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { Hello } from "./hello";
import { rootReducer } from "./store/store";

interface Props { }
export class App extends React.Component<Props, {}> {
    public store: ReturnType<typeof createStore>;
    constructor(props: Props) {
        super(props);
        this.store = createStore(rootReducer);
    }
    public render() {
        return (
            <Provider store={this.store} >
                <Hello />
            </Provider>
        );
    }
}

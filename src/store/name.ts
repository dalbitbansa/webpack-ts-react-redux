import { Actions, ActionTypes } from "./actions";

export interface ChangeNameAction {
    payload: string;
    type: ActionTypes.changeName;
}
export function changeName(name: string): ChangeNameAction {
    return {
        payload: name,
        type: ActionTypes.changeName,
    };
}

export function nameReducer(
    state = "world",
    { type, payload }: Actions,
): string {
    switch (type) {
        case ActionTypes.changeName:
            return payload;
        default:
            return state;
    }
}

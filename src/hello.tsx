import * as React from "react";
import { connect } from "react-redux";
import { changeName } from "./store/name";
import { State } from "./store/store";

interface Props {
    name: string;
    changeName: typeof changeName;
}
class HelloComponent extends React.Component<Props, {}> {
    public render() {
        return (
            <div>
                <h1>Hello, {this.props.name}!</h1>
                <label>
                    Name:&nbsp;
                    <input
                        value={this.props.name}
                        onChange={(e) => this.props.changeName(e.currentTarget.value)}
                    />
                </label>
            </div>
        );
    }
}

export const Hello = connect(
    ({ name }: State) => ({ name }),
    { changeName },
)(HelloComponent);
